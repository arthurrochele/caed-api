const ItemDiscursivo = require('../api/itemDiscursivo/itemDiscursivo');
//const Correcao = require('../api/correcao/correcao');
const _ = require('lodash');

function populationInit() {
  var dominio = new Array();
  dominio.push(
    {
      id: 9859662,
      item: 'D020006H6',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '68300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
      ordem: 1,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
      ],
    },
    {
      id: 9859663,
      item: 'D320006C8',
      referencia: 'upload/correcao_9859663.png',
      sequencial: '68300003130127',
      solicitacao: '2000000886',
      situacao: 'DISPONIVEL',
      ordem: 2,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 187,
          titulo: 'Chave de correção 2',
          opcoes: [
            {
              valor: '0',
              descricao: 'Lido',
            },
            {
              valor: '1',
              descricao: 'Não lido',
            },
          ],
        },
      ],
    },
    {
      id: 9859664,
      item: 'D6530006C7',
      referencia: 'upload/correcao_9859664.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'RESERVADA',
      ordem: 3,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    },
    {
      id: 9859665,
      item: 'D020006H7',
      referencia: 'upload/correcao_9859665.png',
      sequencial: '68300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
      ordem: 4,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
      ],
    },
    {
      id: 9859666,
      item: 'D320006C9',
      referencia: 'upload/correcao_9859666.png',
      sequencial: '68300003130127',
      solicitacao: '2000000886',
      situacao: 'DISPONIVEL',
      ordem: 5,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 187,
          titulo: 'Chave de correção 2',
          opcoes: [
            {
              valor: '0',
              descricao: 'Lido',
            },
            {
              valor: '1',
              descricao: 'Não lido',
            },
          ],
        },
      ],
    },
    {
      id: 9859667,
      item: 'D6530006D1',
      referencia: 'upload/correcao_9859667.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'CORRIGIDA',
      ordem: 6,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    },
    {
      id: 9859668,
      item: 'D6530006D2',
      referencia: 'upload/correcao_9859668.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'RESERVADA',
      ordem: 7,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    },
    {
      id: 9859669,
      item: 'D020006D3',
      referencia: 'upload/correcao_9859669.png',
      sequencial: '68300003130128',
      solicitacao: '2000000885',
      situacao: 'COM_DEFEITO',
      ordem: 8,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
      ],
    },
    {
      id: 9859670,
      item: 'D320006D4',
      referencia: 'upload/correcao_9859670.png',
      sequencial: '68300003130127',
      solicitacao: '2000000886',
      situacao: 'COM_DEFEITO',
      ordem: 9,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 187,
          titulo: 'Chave de correção 2',
          opcoes: [
            {
              valor: '0',
              descricao: 'Lido',
            },
            {
              valor: '1',
              descricao: 'Não lido',
            },
          ],
        },
      ],
    },
    {
      id: 9859671,
      item: 'D6530006D5',
      referencia: 'upload/correcao_985971.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'CORRIGIDA',
      ordem: 10,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    }
  );

  ItemDiscursivo.create(dominio, function (error) {
    if (error) {
      console.log(error);
    }
  });
}

ItemDiscursivo.count(function (error, value) {
  if (error) {
    console.log(error);
  } else {
    if (value == 0) populationInit();
  }
});
