const express = require('express');

module.exports = function (server) {
  // API Routes
  const router = express.Router();
  server.use('/', router);

  // rotas da API
  const itemDiscursivoService = require('../api/itemDiscursivo/itemDiscursivoService');
  itemDiscursivoService.register(router, '/itemDiscursivo');
  router.put('/itemDiscursivo/:idItem', itemDiscursivoService.atualizarItemDiscursivo);
  router.delete('/itemDiscursivo/:idItem', itemDiscursivoService.removerItemDiscursivo);

  const correcaoBuscarItemService = require('../api/correcao/correcaoBuscarItemService');
  correcaoBuscarItemService.register(router, '/correcoes');

  const correcaoCorrigirItemService = require('../api/correcao/correcaoCorrigirItemService');
  router.post('/correcoes/:idCorrecao', correcaoCorrigirItemService.corrigirItem);

  const correcaoReservarItemService = require('../api/correcao/correcaoReservarItemService');
  router.post('/correcoes/reservadas/:idCorrecao', correcaoReservarItemService.reservarItem);

  const correcaoMarcarDefeituosoItemService = require('../api/correcao/correcaoMarcarDefeituosoItemService');
  router.post('/correcoes/defeituoso/:idCorrecao', correcaoMarcarDefeituosoItemService.marcarDefeituoso);
};
