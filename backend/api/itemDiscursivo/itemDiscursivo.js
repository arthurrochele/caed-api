const restful = require('node-restful');
const mongooseHidden = require('mongoose-hidden')({ hidden: { _id: true, _v: true } });
const chaveCorrecao = require('../chaveCorrecao/chaveCorrecao');
const mongoose = restful.mongoose;
mongoose.Promise = global.Promise;

const itemDiscursivo = new mongoose.Schema({
  id: { type: Number, required: true },
  item: { type: String, required: true },
  referencia: { type: String, required: true },
  sequencial: { type: String, required: true },
  solicitacao: { type: String, required: true },
  situacao: { type: String, required: true, uppercase: true, enum: ['DISPONIVEL', 'RESERVADA', 'CORRIGIDA', 'COM_DEFEITO'] },
  ordem: { type: Number, required: true },
  chave: [chaveCorrecao],
});

itemDiscursivo.plugin(mongooseHidden);

module.exports = restful.model('ItensDiscursivos', itemDiscursivo);
