const _ = require('lodash');
const ItemDiscursivo = require('./itemDiscursivo');

ItemDiscursivo.methods(['post']);
ItemDiscursivo.updateOptions({ new: true, runValidators: true });

ItemDiscursivo.after('post', sendErrorsOrNextPost).after('delete', sendErrorsOrNextDelete);

function sendErrorsOrNextPost(req, res, next) {
  const bundle = res.locals.bundle;

  if (bundle.errors) {
    var errors = parseErrors(bundle.errors);
    res.status(500).json({ errors });
  } else {
    res.status(200).json({ resposta: 'Item criado com sucesso' });
  }
}

function sendErrorsOrNextDelete(req, res, next) {
  const bundle = res.locals.bundle;

  if (bundle.errors) {
    var errors = parseErrors(bundle.errors);
    res.status(500).json({ errors });
  } else {
    res.status(200).json({ resposta: 'Item removido com sucesso' });
  }
}

function parseErrors(nodeRestfulErrors) {
  const errors = [];
  _.forIn(nodeRestfulErrors, (error) => errors.push(error.message));
  return errors;
}

ItemDiscursivo.route('count', function (req, res, next) {
  ItemDiscursivo.count(function (error, value) {
    if (error) {
      res.status(500).json({ errors: [error] });
    } else {
      res.json({ value });
    }
  });
});

ItemDiscursivo.route('get', function (req, res, next) {
  ItemDiscursivo.find()
    .sort({ ordem: 1 })
    .exec(function (error, listaItensDiscursivos) {
      if (error) {
        res.status(500).json({ errors: [error] });
      } else {
        res.json({ listaItensDiscursivos });
      }
    });
});

ItemDiscursivo.atualizarItemDiscursivo = function (req, res, next) {
  ItemDiscursivo.update({ id: { $eq: req.params.idItem } }, req.body, function (error, resultado) {
    if (error) {
      res.status(500).json({ errors: [error] });
    } else if (resultado.ok) {
      res.json({ resposta: 'Item atualizado com sucesso' });
    } else {
      res.json({ resposta: 'Falha ao atualizar o item' });
    }
  });
};

ItemDiscursivo.removerItemDiscursivo = function (req, res, next) {
  ItemDiscursivo.remove({ id: { $eq: req.params.idItem } }, function (error) {
    if (error) {
      res.status(500).json({ errors: [error] });
    } else {
      res.json({ resposta: 'Item removido com sucesso' });
    }
  });
};

module.exports = ItemDiscursivo;
