const Correcao = require('./correcao');
const ItemDiscursivo = require('../itemDiscursivo/itemDiscursivo');

function atualizarItemDiscursivo(id, resolve) {
  ItemDiscursivo.update({ id: { $eq: id } }, { situacao: 'COM_DEFEITO' }, (err) => {
    if (err) console.log('Erro ao atualizar o item discursivo!');
    resolve();
  });
}

function marcarDefeituoso(req, res) {
  ItemDiscursivo.findOne({ id: req.params.idCorrecao }).exec(function (error, data) {
    if (error) {
      res.status(500).json({
        situacao: 'ERRO',
        tipo: 'ERRO_INTERNO_SERVIDOR',
        descrição: 'Erro interno do servidor',
      });
    } else if (data) {
      if (data.situacao.localeCompare('CORRIGIDA') == 0) {
        res.json({
          situacao: 'ERRO',
          tipo: 'ITEM_CORRIGIDO',
          descrição: 'Item já corrigido',
        });
      } else if (data.situacao.localeCompare('RESERVADA') == 0) {
        //ramo reservada fechado
        //atualizar item discursivo somente
        var promAtualizarItemDiscursivo = new Promise((resolve, reject) => {
          atualizarItemDiscursivo(req.params.idCorrecao, resolve);
        });

        promAtualizarItemDiscursivo.then(() => {
          res.json({
            situacao: 'SUCESSO',
            descrição: 'Correção marcada como defeituosa',
          });
        });
      } else if (data.situacao.localeCompare('COM_DEFEITO') == 0) {
        res.json({
          situacao: 'ERRO',
          tipo: 'ITEM_COM_DEFEITO',
          descrição: 'Item marcado como defeituoso previamente',
        });
      } else if (data.situacao.localeCompare('DISPONIVEL') == 0) {
        res.json({
          situacao: 'SUCESSO',
          descrição: 'Correção marcada como defeituosa',
        });
      } else {
        res.json({
          situacao: 'ERRO',
          tipo: 'ITEM_INVALIDO',
          descrição: 'Item inválido para marcar como defeituoso',
        });
      }
    } else {
      res.status(404).json({
        situacao: 'ERRO',
        tipo: 'ITEM_CORRECAO_NAO_EXISTENTE',
        descrição: 'Não foi encontrado na base de dados nenhum item de correção com o id ' + req.params.idCorrecao,
      });
    }
  });
}

module.exports = { marcarDefeituoso };
