const restful = require('node-restful');
const mongooseHidden = require('mongoose-hidden')({ hidden: { _id: true, _v: true } });
const chaveCorrecao = require('../chaveCorrecao/chaveCorrecao');
const mongoose = restful.mongoose;
mongoose.Promise = global.Promise;

const correcao = new mongoose.Schema({
  idItemDiscursivo: { type: Number, required: true },
  situacao: { type: String, required: true, uppercase: true, enum: ['CORRIGIDA', 'COM_DEFEITO', 'RESERVADA'] },
  chave: [chaveCorrecao],
});

correcao.plugin(mongooseHidden);

module.exports = restful.model('Correcoes', correcao);
