const _ = require('lodash');
const Correcao = require('./correcao');
const ItemDiscursivo = require('../itemDiscursivo/itemDiscursivo');

let existemDisp;
let proximaFilaItem;

function buscarDisponiveis(resolve) {
  ItemDiscursivo.findOne({ situacao: 'DISPONIVEL' })
    .sort({ ordem: 1 })
    .exec(function (error, data) {
      if (error) {
        existemDisp = 0;
        resolve();
      } else if (data) {
        proximaFilaItem = data;
        existemDisp = 1;
        resolve();
      } else {
        existemDisp = 0;
        resolve();
      }
    });
}

function buscarReservadas(resolve) {
  ItemDiscursivo.findOne({ situacao: 'RESERVADA' })
    .sort({ ordem: 1 })
    .exec(function (error, data) {
      if (data) {
        proximaFilaItem = data;
        resolve();
      } else {
        proximaFilaItem = [];
        resolve();
      }
    });
}

function proximoItemFila(resolve) {
  var promiseBuscaDisp = new Promise((resolve, reject) => {
    buscarDisponiveis(resolve);
  });

  promiseBuscaDisp.then(() => {
    if (!existemDisp) {
      var promiseBuscaReserv = new Promise((resolve, reject) => {
        buscarReservadas(resolve);
      });
      promiseBuscaReserv.then(() => {
        resolve();
      });
    } else {
      resolve();
    }
  });
}

function processarResponder(req, res, idItem, idsChave, arrayMapeamento, situacaoItem) {
  if (situacaoItem && situacaoItem.localeCompare('COM_DEFEITO') == 0) {
    res.json({
      situacao: 'ERRO',
      tipo: 'ITEM_COM_DEFEITO',
      descrição: 'Item marcado como defeituoso',
    });
  }
  if (req.body.chave.length > 1) {
    res.json({
      data: null,
      situacao: 'ERRO',
      tipo: 'MULTIPLAS_CHAVES_INFORMADAS',
      descrição: 'Por favor, informe apenas uma chave de correção',
    });
  }
  if (idsChave.includes(parseInt(req.body.chave[0].id))) {
    var opcaoValida = false;
    _.forEach(arrayMapeamento, (arrayChaveOpcao) => {
      if (arrayChaveOpcao[0] == parseInt(req.body.chave[0].id)) {
        _.forEach(arrayChaveOpcao, (opcao) => {
          if (req.body.chave[0].valor == parseInt(opcao)) {
            opcaoValida = true;
          }
        });
      }
    });
    if (opcaoValida) {
      // ramo do prox item da fila satisfeito
      //gravar correção e atualizar item discursivo - fazer promise
      var promGravarCorrecao = new Promise((resolve, reject) => {
        gravarCorrecao(idItem, req.body.chave[0].id, req.body.chave[0].valor, resolve);
      });

      promGravarCorrecao.then(() => {
        var promAtualizarItemDiscursivo = new Promise((resolve, reject) => {
          atualizarItemDiscursivo(idItem, resolve);
        });

        promAtualizarItemDiscursivo.then(() => {
          res.json({
            situacao: 'SUCESSO',
            descrição: 'Correção salva com sucesso',
          });
        });
      });
    } else {
      res.json({
        situacao: 'ERRO',
        tipo: 'CHAVE_INCORRETA',
        descrição: 'Chave de correção incorreta. Valor ' + req.body.chave[0].valor + ' não é válido para o item ' + req.body.chave[0].id,
      });
    }
  } else {
    res.json({
      situacao: 'ERRO',
      tipo: 'CHAVE_INEXISTENTE',
      descrição: 'Id de Chave de correção não encontrada para a correção de id ' + idItem,
    });
  }
}

function gravarCorrecao(idItem, chave, valor, resolve) {
  var correcao = new Correcao({ idItemDiscursivo: idItem, situacao: 'CORRIGIDA', chave: [{ id: chave, opcoes: [{ valor: valor }] }] });
  correcao.save(function (err) {
    if (err) console.log('Erro ao gravar a correção!');
    resolve();
  });
}

function atualizarItemDiscursivo(id, resolve) {
  ItemDiscursivo.update({ id: { $eq: id } }, { situacao: 'CORRIGIDA' }, (err) => {
    if (err) console.log('Erro ao atualizar o item discursivo!');
    resolve();
  });
}

function corrigirItem(req, res) {
  var proxItem = new Promise((resolve, reject) => {
    proximoItemFila(resolve);
  });

  proxItem.then(() => {
    var resultado = JSON.stringify({ proximaFilaItem });
    resultado = JSON.parse(resultado);
    var idItem = resultado.proximaFilaItem.id;
    var idsChave = new Array();
    var arrayOpcoesChave = new Array();
    var arrayMapeamento = new Array();
    _.forEach(resultado.proximaFilaItem.chave, (chave) => {
      idsChave.push(chave.id);
      arrayOpcoesChave = new Array();
      arrayOpcoesChave.push(chave.id);
      _.forEach(chave.opcoes, (opcao) => {
        arrayOpcoesChave.push(opcao.valor);
      });
      arrayMapeamento.push(arrayOpcoesChave);
    });

    if (idItem == req.params.idCorrecao) {
      processarResponder(req, res, idItem, idsChave, arrayMapeamento, resultado.proximaFilaItem.situacao);
    } else {
      ItemDiscursivo.findOne({ id: req.params.idCorrecao }).exec(function (error, data) {
        if (error) {
          res.status(500).json({
            situacao: 'ERRO',
            tipo: 'ERRO_INTERNO_SERVIDOR',
            descrição: 'Erro interno do servidor',
          });
        } else if (data) {
          if (data.situacao.localeCompare('CORRIGIDA') == 0) {
            res.json({
              situacao: 'ERRO',
              tipo: 'ITEM_CORRIGIDO',
              descrição: 'Item já corrigido',
            });
          } else if (data.situacao.localeCompare('COM_DEFEITO') == 0) {
            res.json({
              situacao: 'ERRO',
              tipo: 'ITEM_COM_DEFEITO',
              descrição: 'Item marcado como defeituoso',
            });
          } else if (data.situacao.localeCompare('RESERVADA') != 0) {
            res.json({
              situacao: 'ERRO',
              tipo: 'ITEM_INVALIDO',
              descrição: 'Item inválido para correção',
            });
          } else {
            var resultado = JSON.stringify({ data });
            resultado = JSON.parse(resultado);
            var idItem = resultado.data.id;
            var idsChave = new Array();
            var arrayOpcoesChave = new Array();
            var arrayMapeamento = new Array();
            _.forEach(resultado.data.chave, (chave) => {
              idsChave.push(chave.id);
              arrayOpcoesChave = new Array();
              arrayOpcoesChave.push(chave.id);
              _.forEach(chave.opcoes, (opcao) => {
                arrayOpcoesChave.push(opcao.valor);
              });
              arrayMapeamento.push(arrayOpcoesChave);
            });

            if (req.body.chave.length > 1) {
              res.json({
                data: null,
                situacao: 'ERRO',
                tipo: 'MULTIPLAS_CHAVES_INFORMADAS',
                descrição: 'Por favor, informe apenas uma chave de correção',
              });
            }
            if (idsChave.includes(parseInt(req.body.chave[0].id))) {
              var opcaoValida = false;
              _.forEach(arrayMapeamento, (arrayChaveOpcao) => {
                if (arrayChaveOpcao[0] == parseInt(req.body.chave[0].id)) {
                  _.forEach(arrayChaveOpcao, (opcao) => {
                    if (req.body.chave[0].valor == parseInt(opcao)) {
                      opcaoValida = true;
                    }
                  });
                }
              });
              if (opcaoValida) {
                // ramo do item reservado satisfeito
                //gravar correção e atualizar item discursivo - fazer promise
                var promGravarCorrecao = new Promise((resolve, reject) => {
                  gravarCorrecao(idItem, req.body.chave[0].id, req.body.chave[0].valor, resolve);
                });

                promGravarCorrecao.then(() => {
                  var promAtualizarItemDiscursivo = new Promise((resolve, reject) => {
                    atualizarItemDiscursivo(idItem, resolve);
                  });

                  promAtualizarItemDiscursivo.then(() => {
                    res.json({
                      situacao: 'SUCESSO',
                      descrição: 'Correção salva com sucesso',
                    });
                  });
                });
              } else {
                res.json({
                  situacao: 'ERRO',
                  tipo: 'CHAVE_INCORRETA',
                  descrição: 'Chave de correção incorreta. Valor ' + req.body.chave[0].valor + ' não é válido para o item ' + req.body.chave[0].id,
                });
              }
            } else {
              res.json({
                situacao: 'ERRO',
                tipo: 'CHAVE_INEXISTENTE',
                descrição: 'Id de Chave de correção não encontrada para a correção de id ' + idItem,
              });
            }
          }
        } else {
          res.status(404).json({
            situacao: 'ERRO',
            tipo: 'ITEM_CORRECAO_NAO_EXISTENTE',
            descrição: 'Não foi encontrado na base de dados nenhum item de correção com o id ' + req.params.idCorrecao,
          });
        }
      });
    }
  });
}

module.exports = { corrigirItem };
