const _ = require('lodash');
const Correcao = require('./correcao');
const ItemDiscursivo = require('../itemDiscursivo/itemDiscursivo');

let existemDisp;

function buscarDisponiveis(req, res, resolve) {
  ItemDiscursivo.findOne({ situacao: 'DISPONIVEL' })
    .sort({ ordem: 1 })
    .exec(function (error, data) {
      if (error) {
        res.status(500).json({ errors: [error] });
        existemDisp = 0;
        resolve();
      } else if (data) {
        var situacao = 'SUCESSO';
        res.json({ data, situacao });
        existemDisp = 1;
        resolve();
      } else {
        existemDisp = 0;
        resolve();
      }
    });
}

function buscarReservadas(req, res, parFim) {
  ItemDiscursivo.findOne({ situacao: 'RESERVADA' })
    .sort({ ordem: 1 })
    .exec(function (error, data) {
      if (error) {
        res.status(500).json({ errors: [error] });
      } else {
        if (data) {
          var situacao = 'SUCESSO';
          res.json({ data, situacao });

          return 1;
        } else if (parFim) {
          res.status(404).json({
            data: null,
            situacao: 'ERRO',
            tipo: 'SEM_CORRECAO_RESERVADA',
            descrição: 'Por favor, tente buscar por uma correção com status DISPONIVEL',
          });

          return 0;
        } else {
          res.status(404).json({
            data: null,
            situacao: 'ERRO',
            tipo: 'SEM_CORRECAO',
            descrição: 'Não existem mais correções disponíveis',
          });

          return 0;
        }
      }
    });
}

Correcao.route('count', function (req, res, next) {
  Correcao.count(function (error, value) {
    if (error) {
      res.status(500).json({ errors: [error] });
    } else {
      res.json({ value });
    }
  });
});

Correcao.route('get', function (req, res, next) {
  Correcao.find(function (error, listaCorrecoes) {
    if (error) {
      res.status(500).json({ errors: [error] });
    } else {
      res.json({ listaCorrecoes });
    }
  });
});

Correcao.route('proxima', function (req, res, next) {
  queryParams = req.query;
  var satisfiedPar = false;
  if (queryParams.reservada === 'true') {
    satisfiedPar = true;
    buscarReservadas(req, res, true);
  } else if (queryParams.reservada === 'false') {
    satisfiedPar = true;
    var promiseBuscaDisp = new Promise((resolve, reject) => {
      buscarDisponiveis(req, res, resolve);
    });
    promiseBuscaDisp.then(() => {
      if (!existemDisp) {
        buscarReservadas(req, res, false);
      }
    });
  } else {
    if (!satisfiedPar) {
      res.json({
        data: null,
        situacao: 'ERRO',
        tipo: 'PARAMETRO_INVALIDO',
        descrição: 'Por favor, verifique o parâmetro informado na query da requisição',
      });
    }
  }
});

Correcao.route('reservadas', function (req, res, next) {
  ItemDiscursivo.find({ situacao: 'RESERVADA' })
    .sort({ ordem: 1 })
    .exec(function (error, data) {
      if (error) {
        res.status(500).json({ errors: [error] });
      } else if (data) {
        if (data.length > 0) {
          res.json({ data, situacao: 'SUCESSO' });
        } else {
          res.status(404).json({
            data: null,
            situacao: 'ERRO',
            tipo: 'SEM_CORRECAO_RESERVADA',
            descrição: 'Não existem correções reservadas no sistema',
          });
        }
      }
    });
});

Correcao.route('excluirTodas', function (req, res, next) {
  ItemDiscursivo.deleteMany(function (error) {
    if (error) {
      res.status(500).json({ errors: [error] });
    } else {
      res.status(200).json({
        situacao: 'SUCESSO',
        descrição: 'Correções excluídas com sucesso',
      });
    }
  });
});

function populationInit(req, res) {
  var dominio = new Array();
  dominio.push(
    {
      id: 9859662,
      item: 'D020006H6',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '68300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
      ordem: 1,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
      ],
    },
    {
      id: 9859663,
      item: 'D320006C8',
      referencia: 'upload/correcao_9859663.png',
      sequencial: '68300003130127',
      solicitacao: '2000000886',
      situacao: 'DISPONIVEL',
      ordem: 2,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 187,
          titulo: 'Chave de correção 2',
          opcoes: [
            {
              valor: '0',
              descricao: 'Lido',
            },
            {
              valor: '1',
              descricao: 'Não lido',
            },
          ],
        },
      ],
    },
    {
      id: 9859664,
      item: 'D6530006C7',
      referencia: 'upload/correcao_9859664.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'RESERVADA',
      ordem: 3,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    },
    {
      id: 9859665,
      item: 'D020006H7',
      referencia: 'upload/correcao_9859665.png',
      sequencial: '68300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
      ordem: 4,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
      ],
    },
    {
      id: 9859666,
      item: 'D320006C9',
      referencia: 'upload/correcao_9859666.png',
      sequencial: '68300003130127',
      solicitacao: '2000000886',
      situacao: 'DISPONIVEL',
      ordem: 5,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 187,
          titulo: 'Chave de correção 2',
          opcoes: [
            {
              valor: '0',
              descricao: 'Lido',
            },
            {
              valor: '1',
              descricao: 'Não lido',
            },
          ],
        },
      ],
    },
    {
      id: 9859667,
      item: 'D6530006D1',
      referencia: 'upload/correcao_9859667.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'CORRIGIDA',
      ordem: 6,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    },
    {
      id: 9859668,
      item: 'D6530006D2',
      referencia: 'upload/correcao_9859668.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'RESERVADA',
      ordem: 7,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    },
    {
      id: 9859669,
      item: 'D020006D3',
      referencia: 'upload/correcao_9859669.png',
      sequencial: '68300003130128',
      solicitacao: '2000000885',
      situacao: 'COM_DEFEITO',
      ordem: 8,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
      ],
    },
    {
      id: 9859670,
      item: 'D320006D4',
      referencia: 'upload/correcao_9859670.png',
      sequencial: '68300003130127',
      solicitacao: '2000000886',
      situacao: 'COM_DEFEITO',
      ordem: 9,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 187,
          titulo: 'Chave de correção 2',
          opcoes: [
            {
              valor: '0',
              descricao: 'Lido',
            },
            {
              valor: '1',
              descricao: 'Não lido',
            },
          ],
        },
      ],
    },
    {
      id: 9859671,
      item: 'D6530006D5',
      referencia: 'upload/correcao_985971.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'CORRIGIDA',
      ordem: 10,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    }
  );

  ItemDiscursivo.create(dominio, function (error) {
    if (error) {
      console.log(error);
      res.status(500).json({ erro: 'Erro Interno no Servidor' });
    } else {
      res.json({ situacao: 'SUCESSO', descrição: 'Ambiente de testes preparado com sucesso' });
    }
  });
}

Correcao.route('reset', function (req, res, next) {
  ItemDiscursivo.deleteMany(function () {
    populationInit(req, res);
  });
});

Correcao.route('get', function (req, res, next) {
  Correcao.find(function (error, listaCorrecoes) {
    if (error) {
      res.status(500).json({ errors: [error] });
    } else {
      res.json({ listaCorrecoes });
    }
  });
});

module.exports = Correcao;
