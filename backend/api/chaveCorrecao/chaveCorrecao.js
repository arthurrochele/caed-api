const restful = require('node-restful');
const mongooseHidden = require('mongoose-hidden')({ hidden: { _id: true, _v: true } });
const mongoose = restful.mongoose;
mongoose.Promise = global.Promise;
const opcaoChaveCorrecao = require('../opcaoChaveCorrecao/opcaoChaveCorrecao');

const chaveCorrecao = new mongoose.Schema({
  id: { type: Number, required: true },
  titulo: { type: String, required: false },
  opcoes: [opcaoChaveCorrecao],
});

chaveCorrecao.plugin(mongooseHidden);

module.exports = chaveCorrecao;
