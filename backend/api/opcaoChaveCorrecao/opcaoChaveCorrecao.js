const restful = require('node-restful');
const mongooseHidden = require('mongoose-hidden')({ hidden: { _id: true, _v: true } });
const mongoose = restful.mongoose;
mongoose.Promise = global.Promise;

const opcaoChaveCorrecao = new mongoose.Schema({
  valor: { type: String, required: true },
  descricao: { type: String, required: false },
});

opcaoChaveCorrecao.plugin(mongooseHidden);

module.exports = opcaoChaveCorrecao;
