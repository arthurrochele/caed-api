const chai = require('chai');
const http = require('chai-http'); // Extensão da lib chai p/ simular requisições http
const subSet = require('chai-subset'); // Extensao da lib chai p/ verificar objetos
const asPromised = require('chai-as-promised');
const _ = require('lodash');
const server = require('../config/server');
require('../config/database');
require('../config/routes')(server);
const testeUtil = require('../api/utils/teste_utils');

const ItemDiscursivo = require('../api/itemDiscursivo/itemDiscursivo');
chai.use(http);
chai.use(subSet);
chai.use(asPromised);

describe('Teste das Regras de Salvamento de Correções - Banco de Dados com itens disponíveis, reservados, corrigidos e com defeito', () => {
  before('Prepara o Domínio', function () {
    var dominio = testeUtil.dominio1();
    function preparaDominio() {
      return new Promise((resolve) => {
        ItemDiscursivo.deleteMany(function () {
          ItemDiscursivo.create(dominio, function () {
            resolve(0);
          });
        });
      });
    }

    return chai.expect(preparaDominio()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Tenta salvar um item que não existe no Banco de Dados', () => {
    const respostaEsperada = {
      situacao: 'ERRO',
      tipo: 'ITEM_CORRECAO_NAO_EXISTENTE',
      descrição: 'Não foi encontrado na base de dados nenhum item de correção com o id 464646465464654',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/464646465464654')
          .send({
            chave: [
              {
                id: 186,
                valor: '0',
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(404);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Tenta salvar um item que existe, está na ordem certa, mas com uma chave inexistente', () => {
    const idItem = 9859662;
    const idChave = 999;
    const respostaEsperada = {
      situacao: 'ERRO',
      tipo: 'CHAVE_INEXISTENTE',
      descrição: 'Id de Chave de correção não encontrada para a correção de id ' + idItem,
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/' + idItem)
          .send({
            chave: [
              {
                id: idChave,
                valor: '0',
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Tenta salvar um item que existe, está na ordem certa, mas com uma opção inválida', () => {
    const idChave = 186;
    const opcaoValue = '10';
    const idItem = 9859662;

    const respostaEsperada = {
      situacao: 'ERRO',
      tipo: 'CHAVE_INCORRETA',
      descrição: 'Chave de correção incorreta. Valor ' + opcaoValue + ' não é válido para o item ' + idChave,
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/' + idItem)
          .send({
            chave: [
              {
                id: idChave,
                valor: opcaoValue,
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Tenta salvar um item já marcado com o status de 'CORRIGIDA'", () => {
    const idChave = 186;
    const opcaoValue = '10';
    const idItem = 9859667;

    const respostaEsperada = {
      situacao: 'ERRO',
      tipo: 'ITEM_CORRIGIDO',
      descrição: 'Item já corrigido',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/' + idItem)
          .send({
            chave: [
              {
                id: idChave,
                valor: opcaoValue,
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Tenta salvar um item já marcado com o status de 'COM_DEFEITO'", () => {
    const idChave = 186;
    const opcaoValue = '10';
    const idItem = 9859669;

    const respostaEsperada = {
      situacao: 'ERRO',
      tipo: 'ITEM_COM_DEFEITO',
      descrição: 'Item marcado como defeituoso',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/' + idItem)
          .send({
            chave: [
              {
                id: idChave,
                valor: opcaoValue,
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Tenta salvar um item fora de ordem, porém com todos os outros requisitos válidos', () => {
    const idChave = 186;
    const opcaoValue = '0';
    const idItem = 9859665;

    const respostaEsperada = {
      situacao: 'ERRO',
      tipo: 'ITEM_INVALIDO',
      descrição: 'Item inválido para correção',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/' + idItem)
          .send({
            chave: [
              {
                id: idChave,
                valor: opcaoValue,
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Salva um item com o status 'DISPONIVEL' respeitando todos os requisitos", () => {
    const idChave = 186;
    const opcaoValue = '0';
    const idItem = 9859662;

    const respostaEsperada = {
      situacao: 'SUCESSO',
      descrição: 'Correção salva com sucesso',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/' + idItem)
          .send({
            chave: [
              {
                id: idChave,
                valor: opcaoValue,
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Salva um item com o status de 'RESERVADA' respeitando todos os requisitos, porém que não é a próximo da fila", () => {
    const idChave = 188;
    const opcaoValue = '5';
    const idItem = 9859668;

    const respostaEsperada = {
      situacao: 'SUCESSO',
      descrição: 'Correção salva com sucesso',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/' + idItem)
          .send({
            chave: [
              {
                id: idChave,
                valor: opcaoValue,
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  after(function () {
    console.log('  SALVAMENTO DE CORREÇÕES - FIM DO BLOCO 1 - UTILIZANDO O DOMÍNIO DE DADOS 1');
  });
});
