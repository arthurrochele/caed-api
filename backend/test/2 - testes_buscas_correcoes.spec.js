const chai = require('chai');
const http = require('chai-http'); // Extensão da lib chai p/ simular requisições http
const subSet = require('chai-subset'); // Extensao da lib chai p/ verificar objetos
const asPromised = require('chai-as-promised');
const _ = require('lodash');
const server = require('../config/server');
require('../config/database');
require('../config/routes')(server);
const testeUtil = require('../api/utils/teste_utils');

const ItemDiscursivo = require('../api/itemDiscursivo/itemDiscursivo');
chai.use(http);
chai.use(subSet);
chai.use(asPromised);

describe('Teste das Regras de Recuperação de Correções - Banco de Dados com itens disponíveis, reservados, corrigidos e com defeito', () => {
  before('Prepara o Domínio', function () {
    var dominio = testeUtil.dominio1();
    function preparaDominio() {
      return new Promise((resolve) => {
        ItemDiscursivo.deleteMany(function () {
          ItemDiscursivo.create(dominio, function () {
            resolve(0);
          });
        });
      });
    }

    return chai.expect(preparaDominio()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Busca Próxima Correção com status 'DISPONIVEL'", () => {
    const data = {
      id: 9859662,
      item: 'D020006H6',
      referencia: 'upload/correcao_9859662.png',
      sequencial: '68300003130128',
      solicitacao: '2000000885',
      situacao: 'DISPONIVEL',
      ordem: 1,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
      ],
    };

    const respostaEsperada = { data, situacao: 'SUCESSO' };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/proxima?reservada=false')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Busca Próxima Correção com status 'RESERVADA'", () => {
    const data = {
      id: 9859664,
      item: 'D6530006C7',
      referencia: 'upload/correcao_9859664.png',
      sequencial: '68300003130128',
      solicitacao: '2000000886',
      situacao: 'RESERVADA',
      ordem: 3,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 188,
          titulo: 'Chave de correção 3',
          opcoes: [
            {
              valor: '0',
              descricao: '0',
            },
            {
              valor: '1',
              descricao: '1',
            },
            {
              valor: '2',
              descricao: '2',
            },
            {
              valor: '3',
              descricao: '3',
            },
            {
              valor: '4',
              descricao: '4',
            },
            {
              valor: '5',
              descricao: '5',
            },
          ],
        },
      ],
    };

    const respostaEsperada = { data, situacao: 'SUCESSO' };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/proxima?reservada=true')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  after(function () {
    console.log('  BUSCA DE ITENS - FIM DO BLOCO 1 - UTILIZANDO O DOMÍNIO DE DADOS 1');
  });
});

describe('Teste das Regras de Recuperação de Correções - Banco de Dados vazio', () => {
  before('Prepara o Domínio', function () {
    function preparaDominio() {
      return new Promise((resolve) => {
        ItemDiscursivo.deleteMany(function () {
          resolve(0);
        });
      });
    }

    return chai.expect(preparaDominio()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Busca próxima correção 'DISPONIVEL' quando o banco de dados está vazio", () => {
    const respostaEsperada = {
      data: null,
      situacao: 'ERRO',
      tipo: 'SEM_CORRECAO',
      descrição: 'Não existem mais correções disponíveis',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/proxima?reservada=false')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(404);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Busca próxima correção 'RESERVADA' quando o banco de dados está vazio", () => {
    const respostaEsperada = {
      data: null,
      situacao: 'ERRO',
      tipo: 'SEM_CORRECAO_RESERVADA',
      descrição: 'Por favor, tente buscar por uma correção com status DISPONIVEL',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/proxima?reservada=true')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(404);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Busca de todos as correções com status "RESERVADA" - Quando o banco de dados está vazio', () => {
    const respostaEsperada = {
      data: null,
      situacao: 'ERRO',
      tipo: 'SEM_CORRECAO_RESERVADA',
      descrição: 'Não existem correções reservadas no sistema',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/reservadas')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(404);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  after(function () {
    console.log('  BUSCA DE ITENS - FIM DO BLOCO 2 - UTILIZANDO O BANCO DE DADOS VAZIO');
  });
});

describe('Teste das Regras de Recuperação de Correções - Banco de Dados com todos os itens já corrigidos', () => {
  before('Prepara o Domínio', function () {
    var dominio = testeUtil.dominio2();
    function preparaDominio() {
      return new Promise((resolve) => {
        ItemDiscursivo.deleteMany(function () {
          ItemDiscursivo.create(dominio, function () {
            resolve(0);
          });
        });
      });
    }

    return chai.expect(preparaDominio()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Busca próxima correção 'DISPONIVEL' quando todos os itens já estão corrigidos", () => {
    const respostaEsperada = {
      data: null,
      situacao: 'ERRO',
      tipo: 'SEM_CORRECAO',
      descrição: 'Não existem mais correções disponíveis',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/proxima?reservada=false')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(404);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Busca próxima correção 'RESERVADA' quando todos os itens já estão corrigidos", () => {
    const respostaEsperada = {
      data: null,
      situacao: 'ERRO',
      tipo: 'SEM_CORRECAO_RESERVADA',
      descrição: 'Por favor, tente buscar por uma correção com status DISPONIVEL',
    };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/proxima?reservada=true')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(404);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  after(function () {
    console.log('  BUSCA DE ITENS - FIM DO BLOCO 3 - UTILIZANDO O DOMÍNIO DE DADOS 2');
  });
});

describe('Teste das Regras de Recuperação de Correções - Banco de Dados com todos os itens já corrigidos restando apenas alguns itens reservados', () => {
  before('Prepara o Domínio', function () {
    var dominio = testeUtil.dominio3();
    function preparaDominio() {
      return new Promise((resolve) => {
        ItemDiscursivo.deleteMany(function () {
          ItemDiscursivo.create(dominio, function () {
            resolve(0);
          });
        });
      });
    }

    return chai.expect(preparaDominio()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Busca próxima correção 'DISPONIVEL' quando todos os itens já estão corrigidos e restam apenas itens reservados", () => {
    const data = {
      id: 9859666,
      item: 'D320006C9',
      referencia: 'upload/correcao_9859666.png',
      sequencial: '68300003130127',
      solicitacao: '2000000886',
      situacao: 'RESERVADA',
      ordem: 5,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 187,
          titulo: 'Chave de correção 2',
          opcoes: [
            {
              valor: '0',
              descricao: 'Lido',
            },
            {
              valor: '1',
              descricao: 'Não lido',
            },
          ],
        },
      ],
    };

    const respostaEsperada = { data, situacao: 'SUCESSO' };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/proxima?reservada=false')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it("Busca próxima correção 'RESERVADA' quando todos os itens já estão corrigidos e restam apenas itens reservados", () => {
    const data = {
      id: 9859666,
      item: 'D320006C9',
      referencia: 'upload/correcao_9859666.png',
      sequencial: '68300003130127',
      solicitacao: '2000000886',
      situacao: 'RESERVADA',
      ordem: 5,
      chave: [
        {
          id: 186,
          titulo: 'Chave de correção 1',
          opcoes: [
            {
              valor: '0',
              descricao: 'Certo',
            },
            {
              valor: '1',
              descricao: 'Errado',
            },
          ],
        },
        {
          id: 187,
          titulo: 'Chave de correção 2',
          opcoes: [
            {
              valor: '0',
              descricao: 'Lido',
            },
            {
              valor: '1',
              descricao: 'Não lido',
            },
          ],
        },
      ],
    };

    const respostaEsperada = { data, situacao: 'SUCESSO' };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/proxima?reservada=true')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Busca de todas as correções com status "RESERVADA"', () => {
    const data = new Array();
    data.push(
      {
        id: 9859666,
        item: 'D320006C9',
        referencia: 'upload/correcao_9859666.png',
        sequencial: '68300003130127',
        solicitacao: '2000000886',
        situacao: 'RESERVADA',
        ordem: 5,
        chave: [
          {
            id: 186,
            titulo: 'Chave de correção 1',
            opcoes: [
              {
                valor: '0',
                descricao: 'Certo',
              },
              {
                valor: '1',
                descricao: 'Errado',
              },
            ],
          },
          {
            id: 187,
            titulo: 'Chave de correção 2',
            opcoes: [
              {
                valor: '0',
                descricao: 'Lido',
              },
              {
                valor: '1',
                descricao: 'Não lido',
              },
            ],
          },
        ],
      },
      {
        id: 9859669,
        item: 'D020006D3',
        referencia: 'upload/correcao_9859669.png',
        sequencial: '68300003130128',
        solicitacao: '2000000885',
        situacao: 'RESERVADA',
        ordem: 8,
        chave: [
          {
            id: 186,
            titulo: 'Chave de correção 1',
            opcoes: [
              {
                valor: '0',
                descricao: 'Certo',
              },
              {
                valor: '1',
                descricao: 'Errado',
              },
            ],
          },
        ],
      },
      {
        id: 9859670,
        item: 'D320006D4',
        referencia: 'upload/correcao_9859670.png',
        sequencial: '68300003130127',
        solicitacao: '2000000886',
        situacao: 'RESERVADA',
        ordem: 9,
        chave: [
          {
            id: 186,
            titulo: 'Chave de correção 1',
            opcoes: [
              {
                valor: '0',
                descricao: 'Certo',
              },
              {
                valor: '1',
                descricao: 'Errado',
              },
            ],
          },
          {
            id: 187,
            titulo: 'Chave de correção 2',
            opcoes: [
              {
                valor: '0',
                descricao: 'Lido',
              },
              {
                valor: '1',
                descricao: 'Não lido',
              },
            ],
          },
        ],
      }
    );

    const respostaEsperada = { data, situacao: 'SUCESSO' };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/correcoes/reservadas')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  after(function () {
    console.log('  BUSCA DE ITENS - FIM DO BLOCO 4 - UTILIZANDO O DOMÍNIO DE DADOS 3');
  });
});
