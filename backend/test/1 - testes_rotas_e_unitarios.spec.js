const chai = require('chai');
const http = require('chai-http'); // Extensão da lib chai p/ simular requisições http
const subSet = require('chai-subset'); // Extensao da lib chai p/ verificar objetos
const asPromised = require('chai-as-promised');
const _ = require('lodash');
const server = require('../config/server');
require('../config/database');
require('../config/routes')(server);
const testeUtil = require('../api/utils/teste_utils');

const ItemDiscursivo = require('../api/itemDiscursivo/itemDiscursivo');
let idItem;
chai.use(http);
chai.use(subSet);
chai.use(asPromised);

describe('Teste de algumas endpoints extras criados, teste de criação, atualização e deleção de itens', () => {
  it('Endpoint que reinicia a base de dados com o domínio 1', () => {
    const respostaEsperada = { situacao: 'SUCESSO', descrição: 'Ambiente de testes preparado com sucesso' };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/correcoes/reset/')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Endpoint que busca todas as correções (itens discursivos) cadastrados  na base de dados', () => {
    const listaItensDiscursivos = testeUtil.dominio1();
    const respostaEsperada = { listaItensDiscursivos };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .get('/itemDiscursivo')
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Endpoint que cria um novo item na base de dados', () => {
    //const listaItensDiscursivos = testeUtil.dominio1();
    const respostaEsperada = { resposta: 'Item criado com sucesso' };

    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .post('/itemDiscursivo')
          .send({
            id: 9859672,
            item: 'D6530006D6',
            referencia: 'upload/correcao_985972.png',
            sequencial: '68300003130128',
            solicitacao: '2000000886',
            situacao: 'DISPONIVEL',
            ordem: 11,
            chave: [
              {
                id: 186,
                titulo: 'Chave de correção 1',
                opcoes: [
                  {
                    valor: '0',
                    descricao: 'Certo',
                  },
                  {
                    valor: '1',
                    descricao: 'Errado',
                  },
                ],
              },
              {
                id: 188,
                titulo: 'Chave de correção 3',
                opcoes: [
                  {
                    valor: '0',
                    descricao: '0',
                  },
                  {
                    valor: '1',
                    descricao: '1',
                  },
                  {
                    valor: '2',
                    descricao: '2',
                  },
                  {
                    valor: '3',
                    descricao: '3',
                  },
                  {
                    valor: '4',
                    descricao: '4',
                  },
                  {
                    valor: '5',
                    descricao: '5',
                  },
                ],
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Recupera o último item inserido', function () {
    function recuperarUltimoId() {
      return new Promise((resolve) => {
        ItemDiscursivo.findOne({}, 'id')
          .sort({ ordem: -1 })
          .exec(function (error, data) {
            if (error) {
              console.log('Erro ao buscar último item');
              resolve(1);
            } else {
              resolve(data);
            }
          });
      });
    }
    return chai.expect(recuperarUltimoId()).to.be.fulfilled.then((resultado) => {
      idItem = resultado.id;
    });
  });

  it('Endpoint que atualiza um item na base de dados', () => {
    const respostaEsperada = { resposta: 'Item atualizado com sucesso' };
    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .put('/itemDiscursivo/' + idItem)
          .send({
            id: 9859672,
            item: 'D6530006D6',
            referencia: 'upload/correcao_985972.png',
            sequencial: '68300003130128',
            solicitacao: '2000000886',
            situacao: 'RESERVADA',
            ordem: 11,
            chave: [
              {
                id: 186,
                titulo: 'Chave de correção 1',
                opcoes: [
                  {
                    valor: '0',
                    descricao: 'Certo',
                  },
                  {
                    valor: '1',
                    descricao: 'Errado',
                  },
                ],
              },
              {
                id: 188,
                titulo: 'Chave de correção 3',
                opcoes: [
                  {
                    valor: '0',
                    descricao: '0',
                  },
                  {
                    valor: '1',
                    descricao: '1',
                  },
                  {
                    valor: '2',
                    descricao: '2',
                  },
                  {
                    valor: '3',
                    descricao: '3',
                  },
                  {
                    valor: '4',
                    descricao: '4',
                  },
                  {
                    valor: '5',
                    descricao: '5',
                  },
                ],
              },
            ],
          })
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  it('Endpoint que remove um item na base de dados', () => {
    const respostaEsperada = { resposta: 'Item removido com sucesso' };
    function fazRequisicao() {
      return new Promise((resolve) => {
        chai
          .request(server)
          .delete('/itemDiscursivo/' + idItem)
          .end((err, res) => {
            chai.expect(err).to.be.null;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.an('object');
            //chai.expect(res.body).to.containSubset([ItemDiscursivo]);
            chai.expect(res.body).to.eql(respostaEsperada);
            resolve(0);
            // Verifica se as caracteristicas do objeto itemRecuperado é igual ao item esperado
          });
      });
    }

    return chai.expect(fazRequisicao()).to.be.fulfilled.then((resultado) => {
      chai.expect(resultado).to.eql(0);
    });
  });

  after(function () {
    console.log('  ENDPOINTS EXTRAS E TESTE DE CRUD DE ITENS - FIM DO BLOCO 1');
  });
});
