# API desenvolvida para prova do CAEd

**Versão final** que implementa a API REST com todas as regras e funcionalidades.

## Organização

A aplicação foi organizada em duas pastas: **api** e **config**.

### Config

Basicamente para a aplicação funcionar é necessário configurar:

- Servidor HTTP (Express)
- Rotas para a API REST (Express/Node Restful)
- Conexão com Banco de Dados (Mongoose/MongoDB).

### Api

API REST foi implementada utilizando um módulo node chamado [node-restful](https://github.com/baugarten/node-restful).

## Configuração

1. Instalar os módulos do node utilizando o **npm**.

```sh
$ cd /backend
$ npm i
```

2. Inicializar a aplicação em **modo desenvolvimento** (recomendado para testes).

```sh
$ npm run dev
```

3. Inicializar a aplicação em **modo produção** onde o PM2 fará a administração da mesma.

```sh
$ npm run production
```
